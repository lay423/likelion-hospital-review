package likelion.hospital.reveiw.domain.dto;

import likelion.hospital.reveiw.domain.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class UserJoinRequest {
    private String userName;
    private String password;

    public User toEntity(String userName, String password){
        return User.builder()
                .userName(userName)
                .password(password)
                .build();
    }
}
