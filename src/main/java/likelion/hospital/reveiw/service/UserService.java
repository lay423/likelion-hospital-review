package likelion.hospital.reveiw.service;

import likelion.hospital.reveiw.domain.User;
import likelion.hospital.reveiw.domain.dto.UserDto;
import likelion.hospital.reveiw.domain.dto.UserJoinRequest;
import likelion.hospital.reveiw.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserService {
    private final UserRepository userRepository;

    public UserDto join(UserJoinRequest request) {
        //userName(id) 중복 check
        userRepository.findByUserName(request.getUserName())
                .ifPresent(user -> new RuntimeException("해당 UserName이 중복됩니다."));

        User savedUser = userRepository.save(request.toEntity(request.getUserName(),request.getPassword()));

        return UserDto.builder()
                .id(savedUser.getId())
                .userName(savedUser.getUserName())
                .build();
    }
}
