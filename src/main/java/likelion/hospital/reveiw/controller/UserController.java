package likelion.hospital.reveiw.controller;

import likelion.hospital.reveiw.domain.Response;
import likelion.hospital.reveiw.domain.dto.UserDto;
import likelion.hospital.reveiw.domain.dto.UserJoinRequest;
import likelion.hospital.reveiw.domain.dto.UserJoinResponse;
import likelion.hospital.reveiw.service.UserService;
import lombok.RequiredArgsConstructor;


import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/users")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @GetMapping("/hello")
    public ResponseEntity<String> hello(){
        return ResponseEntity.ok().body("bye");
    }

    @PostMapping("join")
    public Response<UserJoinResponse> join(@RequestBody UserJoinRequest userJoinRequest) {
        UserDto userDto = userService.join(userJoinRequest);
        return Response.success(new UserJoinResponse(userDto.getUserName()));
    }
}
